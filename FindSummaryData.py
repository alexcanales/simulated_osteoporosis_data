import random

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from faker.providers.person.en import Provider


df1 = pd.read_csv("V3_data.csv", thousands=',')
df2 = pd.read_csv("V4_data.csv", thousands=',')

frames = [df1, df2]
data = pd.concat(frames)
data = data.dropna(axis=0, how='all')

data = data[['PatientGender', 'PatientAge', 'bmdtest_weight_units', 'bmdtest_weight', 'bmdtest_height_units', 'bmdtest_height', 'obreak', 'parentbreak', 'smoke', 'alcohol', 'bmdtest_tscore_fn', 'arthritis', 'rxlist']]

print("Mean")
print(data.mean(axis = 0, skipna = True))

print("----------------\n")
print("Mode")
print(data.mode())
print("----------------\n")
print("STD")
print(data.std(axis = 0, skipna = True))