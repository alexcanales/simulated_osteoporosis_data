import random
from random import gauss
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from faker.providers.person.en import Provider

"""Alex Canales
Sheridan College
2021/04/15
"""

import time
start_time = time.time()

"""Load the Data"""
df1 = pd.read_csv("V3_data.csv", thousands=',')
df2 = pd.read_csv("V4_data.csv", thousands=',')
frames = [df1, df2]
data = pd.concat(frames)
data = data.dropna(axis=0, how='all')


"""Make Rows For Genders"""
def random_genders(size, p=None):
    """Generate n-length ndarray of genders."""
    if not p:
        # default probabilities
        p = (0.5, 0.5)
    gender = (1, 2)
    return np.random.choice(gender, size=size, p=p)

"""Make Rows For Ages"""
def random_ages(size, p=None):
    """Generate n-length ndarray of genders."""
    ages = []
    for i in range(size):
        ages.append( int(gauss(69.6667, 12.239)))
    return ages

"""Make Rows For Weights"""
def random_weight(size, p=None):
    """Generate n-length ndarray of genders."""
    weights = []
    for i in range(size):
        weights.append(int(gauss(137, 36)))
    return weights

"""Make Rows For Height"""
def random_height(size, p=None):
    """Generate n-length ndarray of genders."""
    heights = []
    for i in range(size):
        heights.append(int(gauss(68, 4.362175)))
    return heights

"""Make Rows For Imperial Measurement"""
def fill_imperial(size):
    imperials = [2] * size
    return imperials

"""Make Rows For Obreak"""
def random_obreak(size, p=None):
    """Generate n-length ndarray of genders."""
    if not p:
        # default probabilities
        p = (0.1, 0.3, 0.6)
    obreak = (0, 1, 2)
    return np.random.choice(obreak, size=size, p=p)

"""Make Rows For If Parent Broke Bones"""
def random_parentbreak(size, p=None):
    """Generate n-length ndarray of genders."""
    if not p:
        # default probabilities
        p = (0.1, 0.4, 0.5)
    parentbreak = (0, 1, 2)
    return np.random.choice(parentbreak, size=size, p=p)

"""Make Rows For Smoking"""
def random_smoke(size, p=None):
    """Generate n-length ndarray of genders."""
    if not p:
        # default probabilities
        p = (0.7, 0.3)
    smoke = (0, 1)
    return np.random.choice(smoke, size=size, p=p)

"""Make Rows For Alchohol"""
def random_alcohol(size, p=None):
    """Generate n-length ndarray of genders."""
    if not p:
        # default probabilities
        p = (0.5, 0.2, 0.3)
    alchohol = (0, 1, 2)
    return np.random.choice(alchohol, size=size, p=p)

"""Make Rows For Arthritis"""
def random_arthritis(size, p=None):
    """Generate n-length ndarray of genders."""
    if not p:
        # default probabilities
        p = (0.5, 0.5)
    arthritis = (0, 1)
    return np.random.choice(arthritis, size=size, p=p)

"""Make Random Value for TScore"""
def random_BMDTestScore(gender):
    if(gender == 1):
        testScore = random.uniform(0, -4.5)
    else:
        testScore = random.uniform(0, -4)
    return testScore

"""Make Rows For Caroc Risk Level"""
from sympy import symbols
def random_CarocRiskLevel(gender, age, bmdtest_tscore_fn):

    x, y = symbols('x y')
    """Expressions for Low Risk and High Risk and moderate CAROC (Male)"""
    expr1_male = (0.000380952*pow(x,2)) - (0.0371429*x) - 1.59524
    expr2_male = (-0.001*pow(x,2)) +(0.135*x)-8.15

    """Expressions for Low Risk and High Risk and moderate CAROC (Female)"""
    expr1_female = (0.00207998*pow(x,2)) - (0.204897*x) + 2.54214
    expr2_female = (0.00140172*pow(x,2)) - (0.14403*x) - 0.013813

    #Male
    if gender == 1:
        val_1 = expr1_male.subs(x, age)
        val_2 = expr2_male.subs(x, age)
    else:
        val_1 = expr1_female.subs(x, age)
        val_2 = expr2_female.subs(x, age)

    if bmdtest_tscore_fn > val_1:
        return "Low"
    elif bmdtest_tscore_fn < val_2:
        return "High"
    else:
        return "Moderate"




"""Make the Data Frame with features and begin filling data"""
size = 2000
df = pd.DataFrame(columns=['PatientGender', 'PatientAge', 'bmdtest_weight_units', 'bmdtest_weight', 'bmdtest_height_units', 'bmdtest_height', 'obreak', 'parentbreak', 'smoke', 'alcohol', 'arthritis', 'bmdtest_tscore_fn', 'CarocBasalRiskLevel'])


df['PatientGender'] = random_genders(size)
df['PatientAge'] = random_ages(size)
df['bmdtest_weight_units'] = fill_imperial(size)
df['bmdtest_weight'] = random_weight(size)
df['bmdtest_height_units'] = fill_imperial(size)
df['bmdtest_height'] = random_height(size)
df['obreak'] = random_obreak(size)
df['parentbreak'] = random_obreak(size)
df['smoke'] = random_smoke(size)
df['alcohol'] = random_alcohol(size)
df['arthritis'] = random_arthritis(size)
df['bmdtest_tscore_fn'] = [random_BMDTestScore(df.loc[idx, 'PatientGender']) for idx in range(len(df))]
df['CarocBasalRiskLevel'] = [random_CarocRiskLevel(df.loc[idx, 'PatientGender'], df.loc[idx, 'PatientAge'], df.loc[idx, 'bmdtest_tscore_fn']) for idx in range(len(df))]

# Combine all frames
frames = [data, df]
copier_df = pd.concat(frames)
copier_df.replace(r'\s+', np.nan, regex=True)
copier_df.to_csv('simulated-data-file.csv')

print("--- %s seconds ---" % (time.time() - start_time))
